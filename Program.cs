﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;


namespace DesktopOverlay
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmOverlay Overlay = new frmOverlay();
            Overlay.StartPosition = FormStartPosition.Manual;
            Overlay.Location = new System.Drawing.Point((int)System.Windows.SystemParameters.PrimaryScreenWidth - 300, 24);
            Overlay.Countdown = true;
            
            Overlay.Appointments = new List<Appointment>();
            ParseProjectFile("Appointments.xml", out Overlay.Appointments);
            ParseProjectFile("Settings.xml", out Overlay.ProgramInfo);

            Overlay.ShowInTaskbar = false;

            Application.Run(Overlay);
        }

        public static bool ParseProjectFile<type>(string Filename, out type Settings)
        {
            type Project;
            System.IO.Stream ProjectFile;
            ProjectFile = new System.IO.FileStream(Filename,
                                                    System.IO.FileMode.OpenOrCreate,
                                                    System.IO.FileAccess.ReadWrite,
                                                    System.IO.FileShare.Read);
            try
            {
                System.Xml.XmlDocument Document = new System.Xml.XmlDocument();
                Document.Load(ProjectFile);
                Project = Deserialize<type>(Document);
            }
            catch
            {
                Settings = default(type);
                return false;
            }
            finally
            {
                ProjectFile.Close();
            }

            Settings = Project;
            return true;
        }

        public static Typename Deserialize<Typename>(System.Xml.XmlNode RootNode)
        {
            System.Xml.Serialization.XmlSerializer Serizalizer = new System.Xml.Serialization.XmlSerializer(typeof(Typename));
            System.Xml.XmlReader Reader = new System.Xml.XmlNodeReader(RootNode);
            try
            {
                return (Typename)Serizalizer.Deserialize(Reader);
            }
            catch
            {
                throw;
            }
            finally
            {
                Reader.Close();
            }
        }
    }
}
