﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace DesktopOverlay
{
    class Convolution
    {
        [System.Runtime.InteropServices.DllImport("DoConvolution.dll", CallingConvention = System.Runtime.InteropServices.CallingConvention.Cdecl, EntryPoint = "Convolute")]
        public static extern void CConvolution(IntPtr Scan0, int Stride, int Width, int Height);


        public static void Convolute(ref Bitmap Input)
        {
            Rectangle InputRectangle = new Rectangle(new Point(0, 0), Input.Size);
            BitmapData LockedBits = Input.LockBits(InputRectangle, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            CConvolution(LockedBits.Scan0, LockedBits.Stride, LockedBits.Width, LockedBits.Height);
            Input.UnlockBits(LockedBits);

            return;
        }

    }
}
