﻿using System.Xml;

namespace DesktopOverlay
{
    public struct Deadline
    {
        public int Year;
        public int Month;
        public int Day;
        public int Hour;
        public int Minute;
        public int Second;

        public Deadline(int Year, int Month, int Day, int Hour, int Minute, int Second)
        {
            this.Year = Year;
            this.Month = Month;
            this.Day = Day;
            this.Hour = Hour;
            this.Minute = Minute;
            this.Second = Second;
        }

        public Deadline(System.DateTime D)
        {
            Year = D.Year;
            Month = D.Month;
            Day = D.Day;
            Hour = D.Hour;
            Minute = D.Minute;
            Second = D.Second;
        }

        public string ToString(string Format)
        {
            return new System.DateTime(Year, Month, Day, Hour, Minute, Second).ToString(Format);
        }

        public static bool operator <(Deadline D1, Deadline D2)
        {
            if (D1.Year > D2.Year) return false;
            else if (D1.Year == D2.Year && D1.Month > D2.Month) return false;
            else if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day > D2.Day) return false;
            else if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day == D2.Day && D1.Hour > D2.Hour) return false;
            else if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day == D2.Day && D1.Hour == D2.Hour && D1.Minute > D2.Minute) return false;
            else if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day == D2.Day && D1.Hour == D2.Hour && D1.Minute == D2.Minute && D1.Second > D2.Second) return false;
            else if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day == D2.Day && D1.Hour == D2.Hour && D1.Minute == D2.Minute && D1.Second == D2.Second) return false;

            return true;
        }
        
        public static bool operator ==(Deadline D1, Deadline D2)
        {
            if (D1.Year == D2.Year && D1.Month == D2.Month && D1.Day == D2.Day && D1.Hour == D2.Hour && D1.Minute == D2.Minute && D1.Second == D2.Second)
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Deadline D1, Deadline D2)
        {
            return !(D1 == D2);
        }

        public static bool operator >(Deadline D1, Deadline D2)
        {
            if (!(D1 < D2) && !(D1 == D2)) return true; 
            else return false;
        }

        public System.DateTime ToDateTime()
        {
            return new System.DateTime(Year, Month, Day, Hour, Minute, Second);
        }

        public static Deadline Now()
        {
            return new Deadline(System.DateTime.Now);
        }
    }

    public struct TimeDiff
    {
        public int Day;
        public int Hour;
        public int Minute;
        public int Second;
        
        public TimeDiff(int Day, int Hour, int Minute, int Second)
        {
            this.Day = Day;
            this.Hour = Hour;
            this.Minute = Minute;
            this.Second = Second;
        }

        public string ToString(string Format)
        {
            return new System.TimeSpan(Day, Hour, Minute, Second).ToString(Format);
        }

        static public Deadline operator +(Deadline D, TimeDiff T)
        {
            return new Deadline(new System.DateTime(D.Year, D.Month, D.Day, D.Hour, D.Minute, D.Second).Add(new System.TimeSpan(T.Day, T.Hour, T.Minute, T.Second)));
        }

        static public Deadline operator -(Deadline D, TimeDiff T)
        {
            return new Deadline(new System.DateTime(D.Year, D.Month, D.Day, D.Hour, D.Minute, D.Second).Subtract(new System.TimeSpan(T.Day, T.Hour, T.Minute, T.Second)));
        }

        static public TimeDiff operator *(TimeDiff T, int n)
        {
            TimeDiff Result = T;
            Result.Second *= n;
            Result.Minute += (int)System.Math.Floor((double)T.Second / 60.0d);
            Result.Minute *= n;
            Result.Hour += (int)System.Math.Floor((double)T.Minute / 60.0d);
            Result.Hour *= n;
            Result.Day += (int)System.Math.Floor((double)T.Hour / 24.0d);
            Result.Day *= n;
            return Result;
        }

        public System.TimeSpan ToTimeSpan()
        {
            return new System.TimeSpan(Day, Hour, Minute, Second);
        }

        static public TimeDiff operator *(int n, TimeDiff T)
        {
            TimeDiff Result = T;
            Result.Second *= n;
            Result.Minute += (int)System.Math.Floor((double)T.Second / 60.0d);
            Result.Minute *= n;
            Result.Hour += (int)System.Math.Floor((double)T.Minute / 60.0d);
            Result.Hour *= n;
            Result.Day += (int)System.Math.Floor((double)T.Hour / 24.0d);
            Result.Day *= n;
            return Result;
        }
    }

    public struct Appointment
    {
        public string Name;
        public Deadline FirstDate;
        public bool Recurs;
        public TimeDiff RecurTime;
        public TimeDiff Duration;
        public int Priority;

        public Appointment(string Name, Deadline D, bool Recurs, TimeDiff RecurTime, TimeDiff Duration, int Priority = 0)
        {
            this.Name = Name;
            FirstDate = D;
            this.Recurs = Recurs;
            this.RecurTime = RecurTime;
            this.Duration = Duration;
            this.Priority = Priority;
        }

        public static int compare(Appointment D1, Appointment D2)
        {
            if (D1.FirstDate < D2.FirstDate) return -1;
            else if (D1.FirstDate == D2.FirstDate && D1.Priority < D2.Priority) return -1;
            else if (D1.FirstDate == D2.FirstDate && D1.Priority == D2.Priority) return 0;
            else if (D1.FirstDate == D2.FirstDate && D1.Priority > D2.Priority) return 1;
            else return 1;
        }
    }

    /* Got this from http://www.codeproject.com/Articles/2309/NET-XML-Serialization-a-settings-class#xx555040xx
     * And modified it accordingly */
    public class XmlColor : System.Xml.Serialization.IXmlSerializable
    {
        #region Private Properties

        private System.Drawing.Color color;

        #endregion

        #region Public Properties

        public System.Drawing.Color Color
        {
            get { return color; }
            set { color = value; }
        }

        #endregion

        #region Constructor

        public XmlColor()
            : this(System.Drawing.Color.Black)
        { }

        public XmlColor(System.Drawing.Color color)
        {
            this.color = color;
        }

        #endregion

        #region IXmlSerializable Members

        System.Xml.Schema.XmlSchema System.Xml.Serialization.IXmlSerializable.GetSchema()
        {
            return null;
        }

        void ValidationCallback(object sender, System.Xml.Schema.ValidationEventArgs args)
        {
        }

        void System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteString(this.color.ToArgb().ToString("X8"));
        }

        void System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
        {
            reader.ReadStartElement();
            try
            {
                this.color = System.Drawing.Color.FromArgb(int.Parse(reader.ReadString(), System.Globalization.NumberStyles.HexNumber));
            }
            catch
            {
                this.color = System.Drawing.Color.Black;
            }
            reader.ReadEndElement();
        }

        #endregion // IXmlSerializable Members
    } 

    public struct Settings
    {
        public XmlColor IDBg;
        public XmlColor IHBg;
        public XmlColor IDFg;
        public XmlColor IHFg;
               
        public XmlColor CDBg;
        public XmlColor CHBg;
        public XmlColor CDFg;
        public XmlColor CHFg;
               
        public XmlColor ADBg;
        public XmlColor AHBg;
        public XmlColor ADFg;
        public XmlColor AHFg;
               
        public XmlColor FDBg1;
        public XmlColor FHBg1;
        public XmlColor FDFg1;
        public XmlColor FHFg1;
        public XmlColor FDBg2;
        public XmlColor FHBg2;
        public XmlColor FDFg2;
        public XmlColor FHFg2;

        public TimeDiff CountdownBeforeEvent;
        public TimeDiff ActiveBeforeEvent;

        public int TimerInterval;
    }
}