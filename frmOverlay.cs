﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DesktopOverlay
{
    public partial class frmOverlay : Form
    {

        public frmOverlay()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= 0x00000020 | 0x00080000; // WS_EX_TRANSPARENT

                return createParams;
            }
        }

        public bool Countdown;

        private System.Drawing.Text.PrivateFontCollection Fonts;

        private void frmOverlay_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            this.TopMost = true;
            Fonts = new System.Drawing.Text.PrivateFontCollection();
            
            // Arial (Init)
            IntPtr StreamPointer = System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement( DesktopOverlay.Properties.Resources.arial, 0 );
            Fonts.AddMemoryFont(StreamPointer, DesktopOverlay.Properties.Resources.arial.Length);

            // CONSOLAS (Init)
            StreamPointer = System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement(DesktopOverlay.Properties.Resources.consolas, 0);
            Fonts.AddMemoryFont(StreamPointer, DesktopOverlay.Properties.Resources.consolas.Length);

            // Preparation
            FontArray[0] = new Font(new FontFamily("Arial", Fonts), 24, FontStyle.Regular, GraphicsUnit.Point);
            FontArray[1] = new Font(new FontFamily("Consolas", Fonts), 12, FontStyle.Regular, GraphicsUnit.Point);

            Map = new Bitmap(this.Width, this.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics = Graphics.FromImage(Map);

            Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            UpdateNextAppointment();

            SizeF Sz = Graphics.MeasureString(NextAppointment.Name, FontArray[0]);
            if (Sz.Width > (float)this.Width - 8.0f)
            {
                Width = (int)Sz.Width + 8;
                this.Location = new Point((int)System.Windows.SystemParameters.PrimaryScreenWidth - ( int )this.Width - 44, 24);
                
                Map.Dispose();
                Graphics.Dispose();

                Map = new Bitmap(this.Width, this.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics = Graphics.FromImage(Map);

                Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            }

            Graphics.Clear(Color.Black);
        }

        Bitmap Map, Backbuffer;
        System.Drawing.Graphics Graphics;
        Font[] FontArray = new Font[2];
        public Settings ProgramInfo;
        const int MODE_FLASH = 0;
        const int MODE_ACTIVE = 1;
        const int MODE_COUNTDOWN = 2;
        const int MODE_IDLE = 3;

        public System.Drawing.Color GetColor(bool Foreground, int mode, int phase = 0)
        {
            switch (mode)
            {
                case MODE_FLASH:
                    if (IsInRect)
                    {
                        if (Foreground)
                        {
                            return phase == 1 ? ProgramInfo.FHFg1.Color : ProgramInfo.FHFg2.Color;
                        }
                        else
                        {
                            return phase == 1 ? ProgramInfo.FHBg1.Color : ProgramInfo.FHBg2.Color;
                        }
                    }
                    else
                    {
                        if (Foreground)
                        {
                            return phase == 1 ? ProgramInfo.FDFg1.Color : ProgramInfo.FDFg2.Color;
                        }
                        else
                        {
                            return phase == 1 ? ProgramInfo.FDBg1.Color : ProgramInfo.FDBg2.Color;
                        }
                    }
                    break;
                case MODE_ACTIVE:
                    if (IsInRect)
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.AHFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.AHBg.Color;
                        }
                    }
                    else
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.ADFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.ADBg.Color;
                        }
                    }
                    break;
                case MODE_COUNTDOWN:
                    if (IsInRect)
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.CHFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.CHBg.Color;
                        }
                    }
                    else
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.CDFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.CDBg.Color;
                        }
                    }
                    break;
                case MODE_IDLE:
                    if (IsInRect)
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.IHFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.IHBg.Color;
                        }
                    }
                    else
                    {
                        if (Foreground)
                        {
                            return ProgramInfo.IDFg.Color;
                        }
                        else
                        {
                            return ProgramInfo.IDBg.Color;
                        }
                    }
                    break;
                default: return Color.FromArgb(0);
            }
        }
        
        private void UpdateForm()
        {
            const int ARIAL = 0;
            const int CONSOLAS = 1;

            Color BackgroundColor;

            int nWidth = (int)Math.Max(256, Graphics.MeasureString(NextAppointment.Name, FontArray[0]).Width + 8);

            Map.Dispose();
            Graphics.Dispose();

            Map = new Bitmap(nWidth, 72, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics = Graphics.FromImage(Map);

            Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            if (Countdown)
            {
                if (NextAppointment.FirstDate + NextAppointment.Duration < Deadline.Now())
                {
                    UpdateNextAppointment();
                    
                    Width = (int)Math.Max( 256, Graphics.MeasureString(NextAppointment.Name, FontArray[0]).Width + 8 );
                    this.Location = new Point((int)System.Windows.SystemParameters.PrimaryScreenWidth - (int)this.Width - 44, 24);
                    
                    Map.Dispose();
                    Graphics.Dispose();

                    Map = new Bitmap(this.Width, this.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    Graphics = Graphics.FromImage(Map);

                    Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                }

                if (NextAppointment.FirstDate.ToDateTime().Subtract(DateTime.Now) < new TimeSpan(0, 0, 0, 0, 0))
                {
                    BackgroundColor = DateTime.Now.Millisecond % 500 < 250 ? GetColor(false, MODE_FLASH, 1) : GetColor(false, MODE_FLASH, 2);
                    Graphics.Clear(BackgroundColor);
                    Graphics.DrawString(NextAppointment.Name, FontArray[ARIAL], new SolidBrush(DateTime.Now.Millisecond % 500 < 250 ? GetColor(true, MODE_FLASH, 1) : GetColor(true, MODE_FLASH, 2)), 4.0f, 4.0f);
                    Graphics.DrawString(NextAppointment.FirstDate.ToDateTime().Subtract(DateTime.Now).ToString("hh\\:mm\\:ss"), FontArray[CONSOLAS], new SolidBrush(DateTime.Now.Millisecond % 500 < 250 ? GetColor(true, MODE_FLASH, 1) : GetColor(true, MODE_FLASH, 2)), 4.0f, 48.0f);
                }
                else if (NextAppointment.FirstDate.ToDateTime().Subtract(DateTime.Now) < ProgramInfo.ActiveBeforeEvent.ToTimeSpan())
                {
                    BackgroundColor = GetColor(false, MODE_ACTIVE);
                    Graphics.Clear(BackgroundColor);
                    Graphics.DrawString(NextAppointment.Name, FontArray[ARIAL], new SolidBrush(GetColor(true, MODE_ACTIVE)), 4.0f, 4.0f);
                    Graphics.DrawString(NextAppointment.FirstDate.ToDateTime().Subtract(DateTime.Now).ToString("hh\\:mm\\:ss"), FontArray[CONSOLAS], new SolidBrush(GetColor(true, MODE_ACTIVE)), 4.0f, 48.0f);
                }
                else
                {
                    BackgroundColor = GetColor(false, MODE_COUNTDOWN);
                    Graphics.Clear(BackgroundColor);
                    Graphics.DrawString(NextAppointment.Name, FontArray[ARIAL], new SolidBrush(GetColor(true, MODE_COUNTDOWN)), 4.0f, 4.0f);
                    Graphics.DrawString(NextAppointment.FirstDate.ToDateTime().Subtract(DateTime.Now).ToString("hh\\:mm\\:ss"), FontArray[CONSOLAS], new SolidBrush(GetColor(true, MODE_COUNTDOWN)), 4.0f, 48.0f);
                }
            }
            else
            {
                UpdateNextAppointment();
                BackgroundColor = GetColor(false, MODE_IDLE);
                Graphics.Clear(BackgroundColor);
                Graphics.DrawString("Current Time", FontArray[ARIAL], new SolidBrush(GetColor(true, MODE_IDLE)), 4.0f, 4.0f);
                Graphics.DrawString(DateTime.Now.ToString("HH:mm:ss"), FontArray[CONSOLAS], new SolidBrush(GetColor(true, MODE_IDLE)), 4.0f, 48.0f);
                SizeF Extent = Graphics.MeasureString(DateTime.Now.ToString("yyyy-MM-dd"), FontArray[1]);
                Graphics.DrawString(DateTime.Now.ToString("yyyy-MM-dd"), FontArray[CONSOLAS], new SolidBrush(GetColor(true, MODE_IDLE)), 256.0f - Extent.Width - 4.0f, 48.0f);
            }

            if (IsInRect)
            {
                if( Backbuffer!= null ) Backbuffer.Dispose();
                Backbuffer = (Bitmap)Map.Clone();

                int nlWidth = (int)Math.Max(260, Graphics.MeasureString(NextAppointment.Name, FontArray[0]).Width + 12);

                Map.Dispose();
                Graphics.Dispose();

                Map = new Bitmap(nlWidth, 76, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics = Graphics.FromImage(Map);

                Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                Graphics.Clear(Color.FromArgb(0, BackgroundColor));
                Graphics.DrawImageUnscaledAndClipped(Backbuffer, new Rectangle(new Point( 3, 3 ), Backbuffer.Size));

                Convolution.Convolute(ref Map);

                Width = (int)Math.Max(260, Graphics.MeasureString(NextAppointment.Name, FontArray[0]).Width + 12);
                Height = 76;
                this.Location = new Point((int)System.Windows.SystemParameters.PrimaryScreenWidth - (int)this.Width - 43, 21);
            }
            else
            {
                Width = (int)Math.Max( 256, Graphics.MeasureString(NextAppointment.Name, FontArray[0]).Width + 8 );
                Height = 72;
                this.Location = new Point((int)System.Windows.SystemParameters.PrimaryScreenWidth - (int)this.Width - 44, 24);
                
            }

            SetBitmap(Map, 255);
        }

        public List<Appointment> Appointments;
        public Appointment NextAppointment;
        
        bool IsInRect;
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            IsInRect = new Rectangle(this.Location.X, 24, this.Width, this.Height).Contains(
                Cursor.Position.X,
                Cursor.Position.Y);
            UpdateForm();
            GC.Collect();
        }

        private void UpdateNextAppointment()
        {
            Deadline Now = new Deadline(DateTime.Now);
            List<Appointment> SortedAppointments = new List<Appointment>();

            for (int i = 0; i < Appointments.Count; i++)
            {
                if (Appointments[i].Recurs)
                {
                    bool Found = false;
                    int Repetition = 0;
                    while (!Found)
                    {
                        if (Now + ProgramInfo.CountdownBeforeEvent < Appointments[i].FirstDate + Repetition * Appointments[i].RecurTime)
                        {
                            Found = false;
                            break;
                        }
                        else if (Now - Appointments[i].Duration < Appointments[i].FirstDate + Repetition * Appointments[i].RecurTime)
                        {
                            Found = true;
                            break;
                        }
                        Repetition++;
                    }
                    if (Found)
                    {
                        SortedAppointments.Add(new Appointment(Appointments[i].Name, Appointments[i].FirstDate + Repetition * Appointments[i].RecurTime, false, new TimeDiff(), Appointments[i].Duration));
                    }
                }
                else
                {
                    if (Now > Appointments[i].FirstDate - ProgramInfo.CountdownBeforeEvent && Now < Appointments[i].FirstDate + Appointments[i].Duration)
                    {
                        SortedAppointments.Add(new Appointment(Appointments[i].Name, Appointments[i].FirstDate, false, new TimeDiff(), Appointments[i].Duration));
                    }
                }
            }

            SortedAppointments.Sort(new System.Comparison<Appointment>(Appointment.compare));

            if (SortedAppointments.Count > 0)
            {
                NextAppointment = SortedAppointments[0];
                Countdown = true;
            }
            else
            {
                Countdown = false;
            }

        }

        /* Got this from http://www.codeproject.com/Articles/1822/Per-Pixel-Alpha-Blend-in-C 
         * Portions Copyright © 2002-2004 Rui Godinho Lopes
         */
        public void SetBitmap(Bitmap bitmap, byte opacity)
        {
            if (bitmap.PixelFormat != System.Drawing.Imaging.PixelFormat.Format32bppArgb)
                throw new ApplicationException("The bitmap must be 32ppp with alpha-channel.");

            // The idea of this is very simple,
            // 1. Create a compatible DC with screen;
            // 2. Select the bitmap with 32bpp with alpha-channel in the compatible DC;
            // 3. Call the UpdateLayeredWindow.

            IntPtr screenDc = Win32.GetDC(IntPtr.Zero);
            IntPtr memDc = Win32.CreateCompatibleDC(screenDc);
            IntPtr hBitmap = IntPtr.Zero;
            IntPtr oldBitmap = IntPtr.Zero;

            try
            {
                hBitmap = bitmap.GetHbitmap(Color.FromArgb(0));  // grab a GDI handle from this GDI+ bitmap
                oldBitmap = Win32.SelectObject(memDc, hBitmap);

                Win32.Size size = new Win32.Size(bitmap.Width, bitmap.Height);
                Win32.Point pointSource = new Win32.Point(0, 0);
                Win32.Point topPos = new Win32.Point(Left, Top);
                Win32.BLENDFUNCTION blend = new Win32.BLENDFUNCTION();
                blend.BlendOp = Win32.AC_SRC_OVER;
                blend.BlendFlags = 0;
                blend.SourceConstantAlpha = opacity;
                blend.AlphaFormat = Win32.AC_SRC_ALPHA;

                Win32.UpdateLayeredWindow(Handle, screenDc, ref topPos, ref size, memDc, ref pointSource, 0, ref blend, Win32.ULW_ALPHA);
            }
            finally
            {
                Win32.ReleaseDC(IntPtr.Zero, screenDc);
                if (hBitmap != IntPtr.Zero)
                {
                    Win32.SelectObject(memDc, oldBitmap);
                    //Windows.DeleteObject(hBitmap); // The documentation says that we have to use the Windows.DeleteObject... but since there is no such method I use the normal DeleteObject from Win32 GDI and it's working fine without any resource leak.
                    Win32.DeleteObject(hBitmap);
                }
                Win32.DeleteDC(memDc);
            }
        }
    }

}
